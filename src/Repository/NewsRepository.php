<?php

namespace App\Repository;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, News::class);
    }

    //Home section News
    public function findLastNews($limit): array
    {
        $manager = $this->getEntityManager();
        $query = $manager->createQuery(
            'SELECT n
                FROM \App\Entity\News n
                WHERE n.isPublished = true
                ORDER BY n.id DESC'
        )->setMaxResults($limit);

        return $query->getArrayResult();
    }

    //Find news between two dates
    public function findNewsBetweenDates($from, $to)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.createdAt >= :from')
            ->andWhere('n.createdAt <= :to')
            ->andWhere('n.isPublished = true')
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->orderBy('n.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }
}