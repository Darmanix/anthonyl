<?php

namespace App\Controller;

use App\Form\EditPasswordType;
use App\Form\UserProfileType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class UserController extends AbstractController
{
    private $security;
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    #[Route('/profil', name: 'profil')]
    public function profil(): Response
    {
        return $this->render('user/profil.html.twig');
    }

    #[Route('/edit-profil', name: 'edit_profil')]
    public function editProfil(Request $request, EntityManagerInterface $manager): Response
    {
        $user = $this->security->getUser();
        $form = $this->createForm(UserProfileType::class, $user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            if(empty($user->getImageFile())){
                $user->setImage($user->getImage());
            }
            $user->setUpdatedAt(new \DateTimeImmutable());
            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute('profil');
        }
        return $this->renderForm('user/edit-profil.html.twig',[
            'form' => $form
        ]);

    }

    #[Route('/edit-password', name: 'edit_password')]
    public function editPassword(Request $request, EntityManagerInterface $manager, UserPasswordHasherInterface $passwordHasher): Response
    {
        $user = $this->security->getUser();
        $form = $this->createForm(EditPasswordType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $user->setPassword($passwordHasher->hashPassword($user, $form->get('password')->getData()));
            $user->setUpdatedAt(new \DateTimeImmutable());
            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute('profil');
        }
        return $this->renderForm('user/edit-password.html.twig', [
            'form' => $form
        ]);
    }
}