<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TeamController extends AbstractController
{
    #[Route('/team', name: 'team')]
    public function team(UserRepository $userRepository): Response
    {
        /*
        $users = $userRepository->findBy([
            'roles' => '["ROLE_ADMIN"]'
        ]);
        RETURN EMPTY
        */

        $role = 'ADMIN';
        $admins = $userRepository->findUserByRole($role);

        return $this->render('team/team.html.twig', ['admins' => $admins]);
    }
}
