<?php

namespace App\Controller;

use App\Repository\NewsRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NewsController extends AbstractController
{
    #[Route('/news', name: 'news')]
    public function news(NewsRepository $newsRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $to = new \DateTimeImmutable();
        $from = $to->modify('-1month');
        $news = $newsRepository->findNewsBetweenDates($from, $to);
        $pagination = $paginator->paginate(
            $news,
            $request->query->getInt('page', 1), //Page number
            10 //Limit per page
        );
        return $this->render('news/news.html.twig', ['news' => $pagination]);
    }
}