<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Course;
use App\Form\CommentType;
use App\Repository\CourseCategoryRepository;
use App\Repository\CourseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class CourseController extends AbstractController
{
    private $security;
    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    /**
     * @param CourseCategoryRepository $categoryRepository
     * @param CourseRepository $courseRepository
     * @return Response
     */
    #[Route('/courses', name: 'courses')]
    public function courses(CourseCategoryRepository $categoryRepository, CourseRepository $courseRepository): Response
    {
        $categories = $categoryRepository->findBy(
            [],
            ['name' => 'ASC']
        );
        $courses = $courseRepository->findBy(
            ['isPublished' => true],
            ['name' => 'ASC']
        );
        return $this->render('course/courses.html.twig', [
            'categories' => $categories,
            'courses' => $courses
        ]);
    }

    /**
     * @param Course $course
     * @return Response
     */
    #[Route('/course/{slug}', name: 'course')]
public function course(Course $course, Request $request, EntityManagerInterface $manager): Response
    {
        $user = $this->security->getUser();
        $comments = $manager->getRepository(Comment::class)->findBy([
            'course' => $course
        ]);
        $nbComment = count($comments);
        $totalRating = 0;
        $moyenneRating = 0;
        $userComment = false;

        foreach ($comments as $comment){
            $totalRating += $comment->getRating();
            if($comment->getAuthor() == $user){
                $userComment = true;
            }
        }
        if($nbComment != 0){
            $moyenneRating = round($totalRating / $nbComment, 2);
        }

        //Instanciation d'un objet Commentaire & création du formulaire
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $comment->setAuthor($this->getUser());
            $comment->setCourse($course);
            $comment->setCreatedAt(new \DateTimeImmutable());
            $comment->setIsPublished(true);
            $manager->persist($comment);
            $manager->flush();
            $this->addFlash(
                'success',
                'Votre commentaire a bien été posté!'
            );
            //return $this->redirectToRoute('courses');
        }

        return $this->renderForm('course/course.html.twig', [
            'course' => $course,
            'form' => $form,
            'commentaries' => $comments,
            'nbcomment' => $nbComment,
            'moyenne' => $moyenneRating,
            'usercomment' => $userComment
        ]);
    }
}