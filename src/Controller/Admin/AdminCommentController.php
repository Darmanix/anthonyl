<?php

namespace App\Controller\Admin;

use App\Entity\Comment;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminCommentController extends AbstractController
{
    #[Route('/admin/comments', name: 'admin_comments')]
    public function adminComments(CommentRepository $repository): Response
    {
        $comments = $repository->findBy(
            [],
            ['createdAt' => 'DESC']
        );
        return $this->render('admin/comments/comments.html.twig', [
            'comments' => $comments,
        ]);
    }

    #[Route('/admin/delcomment/{id}', name: 'admin_del_comment')]
    public function delComment(Comment $comment, EntityManagerInterface $manager) :response {
        $manager->remove($comment);
        $manager->flush();
        $this->addFlash(
            'success',
            'Le commentaire '.$comment->getTitle(). ' a bien été supprimé'
        );
        return $this->redirectToRoute('admin_comments');
    }

    #[Route('/admin/viewcomment/{id}', name: 'admin_view_comment')]
    public function viewNews(Comment $comment, EntityManagerInterface $manager): Response
    {
        $comment->setIsPublished(!$comment->getIsPublished());
        $manager->flush();
        return $this->redirectToRoute('admin_comments');
    }
}