<?php

namespace App\Controller\Admin;

use App\Entity\News;
use App\Form\EditNewsType;
use App\Form\NewsType;
use App\Repository\NewsRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminNewsController extends AbstractController
{
    #[Route('/admin/news', name: 'admin_news')]
    public function adminNews(NewsRepository $repository): Response
    {
        $news = $repository->findBy(
            [],
            ['createdAt' => 'DESC']
        );
        return $this->render('admin/news/news.html.twig', [
            'news' => $news,
        ]);
    }

    #[Route('/admin/createnews', name: 'admin_new_news')]
    public function createNews(EntityManagerInterface $manager, Request $request)
    {
        $news = new News();
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $slug = new Slugify();
            $news->setAuthor($this->getUser());
            $news->setSlug($slug->slugify($news->getName()));
            $news->setCreatedAt(new \DateTimeImmutable());
            $news->setUpdatedAt(new \DateTimeImmutable());
            $news->setIsPublished(true);
            $manager->persist($news);
            $manager->flush();
            $this->addFlash(
                'success',
                'La news '.$news->getName().' a bien été ajoutée.'
            );
            return $this->redirectToRoute('admin_news');
        }

        return $this->renderForm('admin/news/createnews.html.twig', [
            'form' => $form
        ]);
    }

    #[Route('/admin/delnews/{id}', name: 'admin_del_news')]
    public function delNews(News $news, EntityManagerInterface $manager) :response {
        $manager->remove($news);
        $manager->flush();
        $this->addFlash(
            'success',
            'La news '.$news->getName(). ' a bien été supprimée'
        );
        return $this->redirectToRoute('admin_news');
    }

    #[Route('/admin/viewnews/{id}', name: 'admin_view_news')]
    public function viewNews(News $news, EntityManagerInterface $manager): Response
    {
        $news->setIsPublished(!$news->getIsPublished());
        $manager->flush();
        return $this->redirectToRoute('admin_news');
    }

    #[Route('/admin/editnews/{id}', name: 'admin_edit_news')]
    public function editCourse(News $news, Request $request, EntityManagerInterface $manager): Response
    {
        $form = $this->createForm(EditNewsType::class, $news);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $slug = new Slugify();
            $news->setSlug($slug->slugify($news->getName()));
            $news->setUpdatedAt(new \DateTimeImmutable());
            $manager->persist($news);
            $manager->flush();
            $this->addFlash(
                'success',
                'La news '.$news->getName(). ' a bien été modifiée'
            );
            return $this->redirectToRoute('admin_news');
        }
        return $this->renderForm('admin/news/editnews.html.twig', [
            'form' => $form
        ]);
    }
}