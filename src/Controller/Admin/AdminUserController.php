<?php

namespace App\Controller\Admin;

use App\Repository\CourseRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\User;

class AdminUserController extends AbstractController
{
    /**
     * @param UserRepository $repository
     * @return Response
     */
    #[Route('/admin/users', name: 'admin_users')]
    public function adminUsers(UserRepository $repository): Response
    {
        $users = $repository->findBy(
            [],
            ['lastName' => 'ASC']
        );
        return $this->render('admin/users/users.html.twig',[
            'users' => $users,
        ]);
    }

    #[Route('/admin/user-promote/{id}/{role}', name: 'admin_user_promote')]
    public function promote(User $user, EntityManagerInterface $manager, $role) {
        $user->setRoles([$role]);
        $manager->flush();
        return $this->redirectToRoute('admin_users');
    }
}