<?php

namespace App\Controller\Admin;

use App\Entity\Course;
use App\Form\CourseType;
use App\Form\EditCourseType;
use Cocur\Slugify\Slugify;
use App\Repository\CourseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminCourseController extends AbstractController
{
    /**
     * @param CourseRepository $repository
     * @return Response
     */
    #[Route('/admin/courses', name: 'admin_courses')]
    public function adminCourses(CourseRepository $repository): Response
    {
        $courses = $repository->findBy(
            [],
            ['createdAt' => 'DESC']
        );
        return $this->render('admin/courses/courses.html.twig',[
            'courses' => $courses,
        ]);
    }

    #[Route('/admin/editcourse/{id}', name: 'admin_edit_course')]
    public function editCourse(Course $course, Request $request, EntityManagerInterface $manager): Response
    {
        $form = $this->createForm(EditCourseType::class, $course);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($course);
            $manager->flush();
            return $this->redirectToRoute('admin_courses');
        }
        return $this->renderForm('admin/courses/editcourse.html.twig', [
            'form' => $form
        ]);
    }

    /**
     * @param Course $course
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/admin/viewcourse/{id}', name: 'admin_view_course')]
    public function viewCourse(Course $course, EntityManagerInterface $manager): Response
    {
        $course->setIsPublished(!$course->getIsPublished());
        $manager->flush();
        return $this->redirectToRoute('admin_courses');
    }

    /**
     * @param Course $course
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/admin/delcourse/{id}', name: 'admin_del_course')]
    public function delCourse(Course $course, EntityManagerInterface $manager) :response {
        $manager->remove($course);
        $manager->flush();
        $this->addFlash(
            'success',
            'Le cours '.$course->getName(). ' a bien été supprimé'
        );
        return $this->redirectToRoute('admin_courses');
    }

    #[Route('/admin/newcourse', name: 'admin_new_course')]
    public function newCourse(EntityManagerInterface $manager, Request $request)
    {
        $course = new Course();
        $form = $this->createForm(CourseType::class, $course);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $slug = new Slugify();
            $course->setSlug($slug->slugify($course->getName()));
            $course->setCreatedAt(new \DateTimeImmutable());
            $course->setIsPublished(true);
            $manager->persist($course);
            $manager->flush();
            $this->addFlash(
                'success',
                'La formation '.$course->getName().' a bien été ajoutée.'
            );
            return $this->redirectToRoute('admin_courses');
        }

        return $this->renderForm('admin/courses/newcourse.html.twig', [
            'form' => $form
        ]);
    }

}