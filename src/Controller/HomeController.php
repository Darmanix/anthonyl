<?php

namespace App\Controller;

use App\Repository\CourseRepository;
use App\Repository\NewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(CourseRepository $courseRepository, NewsRepository $newsRepository): Response
    {
        $maxCourses = 3;
        $maxNews = 4;

        $courses = $courseRepository->findLastCourses($maxCourses);
        $news = $newsRepository->findLastNews($maxNews);
        return $this->render('home/index.html.twig',[
            'courses' => $courses,
            'news' => $news
        ]);
    }
}
