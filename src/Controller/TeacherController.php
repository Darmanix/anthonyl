<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TeacherController extends AbstractController
{
    #[Route('/teacher', name: 'teacher')]
    public function teacher(UserRepository $userRepository): Response
    {
        $role = 'TEACHER';
        $teachers = $userRepository->findUserByRole($role);
        return $this->render('teacher/teacher.html.twig', ['teachers' => $teachers]);
    }
}
