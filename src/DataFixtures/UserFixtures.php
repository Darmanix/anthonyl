<?php

namespace App\DataFixtures;

use App\Entity\User;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private $genders = ['male', 'female'];
    private $hasher;

    public function __construct(UserPasswordHasherInterface $hasher) {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        $slug = new Slugify();
        for ($i = 1; $i <= 50; $i++) {
            $user = new User();
            $gender = $faker->randomElement($this->genders);
            $user->setFirstName($faker->firstName($gender));
            $user->setLastName($faker->lastName);
            $user->setEmail($slug->slugify($user->getFirstName()) . '.' . $slug->slugify($user->getLastName()) . '@gmail.com');
            $gender = $gender == 'male' ? 'm' : 'f';
            $user->setImage('0' . ($i + 10) . $gender . '.jpg');
            $user->setPassword($this->hasher->hashPassword($user, 'password'));
            $user->setCreatedAt(new \DateTimeImmutable());
            $user->setUpdatedAt(new \DateTimeImmutable());
            $user->setLastLogAt(new \DateTimeImmutable());
            $user->setIsDisabled($faker->boolean(10));
            $user->setRoles(['ROLE_USER']);
            $manager->persist($user);

        }
        $manager->flush();

        /* Teachers */
        $teachers = [
            ['firstName' => 'Eikichi', 'lastName' => 'Onizuka', 'image' => 'onizuka.jpg', 'quote' => 'C\'est mon job de rendre l\'école amusante!'],
            ['firstName' => 'Kesuke', 'lastName' => 'Miyagi', 'image' => 'miyagi.jpg', 'quote' => 'L\'homme qui attrape la mouche avec des baguettes, accomplit n\'importe quoi!'],
            ['firstName' => 'Walter', 'lastName' => 'White', 'image' => 'walter.jpg', 'quote' => 'Je suis le danger!'],
            ['firstName' => 'Minerva', 'lastName' => 'McGonagall', 'image' => 'mcgonagall.jpg', 'quote' => 'Cinq points seront attribués à chacun de vous... pour votre chance insolente!']
        ];
        foreach ($teachers as $teacher){
            $user = new User();
            $user->setFirstName($teacher['firstName']);
            $user->setLastName($teacher['lastName']);
            $user->setEmail(strtolower($teacher['firstName']).'.'.strtolower($teacher['lastName']).'@gmail.com');
            $user->setImage($teacher['image']);
            $user->setQuote($teacher['quote']);
            $user->setPassword($this->hasher->hashPassword($user, 'password'));
            $user->setCreatedAt(new \DateTimeImmutable());
            $user->setUpdatedAt(new \DateTimeImmutable());
            $user->setLastLogAt(new \DateTimeImmutable());
            $user->setIsDisabled(false);
            $user->setRoles(['ROLE_TEACHER']);
            $manager->persist($user);
        }

        /* Admins */
        $admins = [
            ['firstName' => 'Dwayne', 'lastName' => 'Johnson', 'image' => 'rock.jpg', 'quote' => 'Laisse moi t\'interrompre avant d\'assumer que j\'en ai quelque chose à foutre!'],
            ['firstName' => 'Kame', 'lastName' => 'Sennin', 'image' => 'kame.jpg', 'quote' => 'Kamehameha!'],
            ['firstName' => 'Joffrey', 'lastName' => 'Lannister', 'image' => 'jof.jpg', 'quote' => 'Ser Ilyn, apportez moi sa tête!'],
            ['firstName' => 'Chuck', 'lastName' => 'Norris', 'image' => 'chuck.jpg', 'quote' => 'Je mets les pieds où je veux Little John... Et c\'est souvent dans la gueule!'],
            ['firstName' => 'Master', 'lastName' => 'Yoda', 'image' => 'yoda.jpg', 'quote' => 'Fais le ou ne le fais pas. Mais il n\'y a pas d\'essai!'],
            ['firstName' => 'John', 'lastName' => 'Doe', 'image' => 'doe.jpg', 'quote' => 'Les médecins disent que j\'ai l\'alzheimer...']
        ];
        foreach ($admins as $admin){
            $user = new User();
            $user->setFirstName($admin['firstName']);
            $user->setLastName($admin['lastName']);
            $user->setEmail(strtolower($admin['firstName']).'.'.strtolower($admin['lastName']).'@gmail.com');
            $user->setImage($admin['image']);
            $user->setQuote($admin['quote']);
            $user->setPassword($this->hasher->hashPassword($user, 'password'));
            $user->setCreatedAt(new \DateTimeImmutable());
            $user->setUpdatedAt(new \DateTimeImmutable());
            $user->setLastLogAt(new \DateTimeImmutable());
            $user->setIsDisabled(false);
            $user->setRoles(['ROLE_ADMIN']);
            $manager->persist($user);
        }
        $manager->flush();

        /* Super Admin Pat Mar */
        $user = new User();
        $user->setFirstName('Pat');
        $user->setLastName('Mar');
        $user->setEmail('pat.mar@gmail.com');

        $user->setImage('074m.jpg');
        $user->setPassword($this->hasher->hashPassword($user, 'password'));
        $user->setCreatedAt(new \DateTimeImmutable());
        $user->setUpdatedAt(new \DateTimeImmutable());
        $user->setLastLogAt(new \DateTimeImmutable());
        $user->setIsDisabled(false);
        $user->setRoles(['ROLE_SUPER_ADMIN']);
        $manager->persist($user);

        $manager->flush();
    }
}