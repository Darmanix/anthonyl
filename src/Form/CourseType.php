<?php

namespace App\Form;

use App\Entity\Course;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom du cours'
            ])
            ->add('smallDescription', TextType::class, [
                'label' => 'Description courte'
            ])
            ->add('fullDescription', CKEditorType::class, [
                'label' => 'Description complète'
            ])
            ->add('duration', IntegerType::class, [
                'label' => 'Durée de la formation'
            ])
            ->add('price', MoneyType::class, [
                'label' => 'Prix de la formation'
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Image du cours',
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false
            ])
            ->add('schedule', TextType::class, [
                'label' => 'Horaire de la formation'
            ])
            ->add('programFile', VichFileType::class, [
                'label' => 'Programme de la formation',
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false
            ])
            ->add('category', EntityType::class, [
                'label' => 'Catégorie de la formation',
                'placeholder' => 'Sélectionnez...',
                'class' => 'App:CourseCategory',
                'choice_label' => 'name',
                'query_builder' => function(EntityRepository $repository) {
                    return $repository->createQueryBuilder('c')->orderBy('c.name', 'ASC');
                }
            ])
            ->add('level', EntityType::class, [
                'label' => 'Niveau de la formation',
                'placeholder' => 'Sélectionnez...',
                'class' => 'App:CourseLevel',
                'choice_label' => 'name'
            ])
            ->add('teacher', EntityType::class, [
                'label' => 'Professeur de la formation',
                'placeholder' => 'Sélectionnez...',
                'class' => 'App:User',
                'choice_label' => 'lastName',
                'query_builder' => function(EntityRepository $repository) {
                    return $repository->createQueryBuilder('u')->orderBy('u.firstName', 'ASC')->andWhere('u.roles LIKE :role')->setParameter('role', '%"ROLE_TEACHER"%');
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Course::class,
        ]);
    }
}