<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class EditPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('password', RepeatedType::class, [
                'type'              => PasswordType::class,
                'invalid_message'   => 'Les mots de passe ne correspondent pas!',
                'options'           => ['attr' => ['class' => 'password-field']],
                'required'          => true,
                'first_options'     => ['label' => 'Votre nouveau mot de passe'],
                'second_options'    => ['label' => 'Confirmez votre mot de passe'],
                'empty_data'        => '',
                'constraints' => [
                    new Length([
                        'min'                   => 6,
                        'minMessage'            => 'Votre mot de passe doit comporter {{ limit }} caractères au minimum',
                        'max'                   => 64,
                    ]),
                    new NotBlank([
                        'message'               => 'Insérez votre mot de passe'
                    ])
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
