<?php

namespace App\Form;

use App\Entity\News;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class EditNewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom de la news'
            ])
            //->add('createdAt')
            //->add('updatedAt')
            ->add('content', TextType::class, [
                'label' => 'Contenu de la news'
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Image de la news',
                'required'          => false,
                'download_label'    =>false,
                'image_uri'         =>false,
                'delete_label'      => false,
                'allow_delete'      => false,
                'attr'              => [
                    "placeholder" => 'Image de la news'
                ]
            ])
            //->add('isPublished')
            //->add('slug')
            //->add('author')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => News::class,
        ]);
    }
}