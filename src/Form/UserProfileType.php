<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class UserProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName', TextType::class, [
                'label' => 'Votre prénom',
                'attr'  => [
                    "placeholder" => 'Votre prénom'
                ]
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Votre nom de famille',
                'attr'  => [
                    "placeholder" => 'Votre nom de famille'
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => 'Votre e-mail',
                'attr'  => [
                    "placeholder" => 'Votre e-mail'
                ]
            ])
            ->add('imageFile', VichImageType::class, [
                'label'             => 'Votre avatar',
                'required'          => false,
                'download_label'    =>false,
                'image_uri'         =>false,
                'delete_label'      => false,
                'allow_delete'      => false,
                'attr'              => [
                    "placeholder" => 'Votre avatar'
                ]
            ])
            ->add('quote', TextareaType::class, [
                'label' => 'Votre description',
                'attr'  => [
                    "placeholder" => 'Votre description'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
